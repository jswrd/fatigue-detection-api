const express = require('express');
const router = express.Router();
const db = require('../services/mysql.db.service');
const state = db.state;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const checkAuth = require('../middleware/check-auth');
const fs = require("fs");
const path = require("path");


router.get("/", (req, res) => {
   const score = req.body;
   console.log(score);
   res.send('Its working!!!');    
});

router.post('/a', checkAuth, (req, res) => {
    console.log(req.body);
    res.send('done!');
    
});

router.post('/image', (req, res) => {
    
    // console.log(req.body.token);

    var id = new Date().getTime();
    
    fs.writeFileSync(`./images/test.jpg`, new Buffer(req.body.image, "base64"), function(err) {
        console.log(err);
        
    });

    fs.renameSync('./images/test.jpg', `./images/test${id}.jpg`);
    fs.readdir('./images', (err, files) => {
        if (err) {
            console.log(err);
        }
    
        files.forEach(file => {
            const fileDir = path.join('./images', file);
            
            
    
            if (file !== `test${id}.jpg`) {
                fs.unlinkSync(fileDir);
            }
                
            //  else {
            //     var name = path.basename(file);
            //     console.log(name);
            // }
        });
    });
    

    
    
    res.send('Done!!');
});


router.post("/", (req, res) => {
    const name = req.body.name;
    const id = req.body.id;
    const run_time = req.body.run_time;
    const fatigue_score = req.body.fatigue_score;
    console.log(id);
    const date = req.body.date;
    const shift_start_time = req.body.shift_start_time;
    const shift_end_time = req.body.shift_end_time;
    console.log(shift_start_time);
    // const score = req.body;
    // const scores = req.body.dashboard_data;
    // console.log(score);
    // console.log(scores);
    
    
    

    let query1 = state.connection.query(`SELECT * FROM employee WHERE id = ${id}`, (err, results, fields) => {
        if(err) {
            console.log(err);
            res.status(404).send("Error");
        } else {
            const employee_id = results[0].id;
            let _fatigue = {
                employee_id: employee_id,
                time: run_time,
                fatigue_score_value: fatigue_score
            };
            state.connection.query(`INSERT INTO fatigue_score SET ?`, [_fatigue], (err, data) => {
                if(err) {
                    console.log(err);
                    res.status(404).send("Error");
                } else {
                    console.log('done fatigue');
                    // res.status(200).send("Fatigue score table is updated!");
                    // res.end();
                
                }
            });
            

        }
    });

    let _date = {
        date: date
    }
    let query2 = state.connection.query('INSERT INTO date SET ?', _date, (err, data) => {
        if(err) {
            console.log(err);
            res.status(404).send("Error");
        } else {
            console.log('done date');
            
            // res.status(200).send("tables are updated!");
            // res.end();
        }
    });

    if(shift_start_time !== "0" || shift_end_time !== "0" ){

        let _duty_hours = {
            employee_id: id,
           start_time: shift_start_time,
           end_time: shift_end_time
    }
    
            let query3 = state.connection.query('INSERT INTO duty_hours SET ?', _duty_hours, (err, result) => {
            if(err) {
                                    console.log(err);
                                    res.status(404).send("Error");
                                } else {
                                    console.log('done duty');
                                    
                                    res.status(200).send("tables are updated!");
                                
                                }
        })
    } else {
        res.status(400).send("Duty hours table is not updated!");
    }


    
    
     
});



router.post('/employee', checkAuth, (req, res) => {
    const scores = req.body;
    console.log(scores);
    res.send('data received!!');
});

router.post('/auth', (req, res) => {
    const username = req.body.username;
    state.connection.query('SELECT * FROM client WHERE username = ?', [username], (error, result, field) => {
        if( result.length > 0){
                bcrypt.compare( req.body.password, result[0].password, (err, results) => {
                if(err){
                    return res.status(401).json({
                        message: 'Login Failed'
                    })
                }
                if(results){

                    const token = jwt.sign({
                        
                        userfullname: result[0].username,
                        
                    }, 
                    'secret',
                    {
                        expiresIn: '1h',
                    }
                    );
                    return res.status(200).json({
                        message: 'Login Successful',
                        token: token,
                        timestamp: Math.floor(Date.now() / 1000),
                        expireTimestamp: Math.floor((Date.now() / 1000) + 3600)
                    })
                }
                res.status(401).json({
                    message: 'Incorrect Password',
                })
            })



        }else{
            res.status(401).json({
                message: 'Incorrect Email.'
            })
        }
        
    })
});

router.post('/addAuth', (req, res, next) => {
    try {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
        if(err) {
            return res.status(500).json({
                error: err,
            });
        } else {
            let user = {
                username: req.body.username,
                password: hash
            }
            let sql = "INSERT INTO client SET ?"
            state.connection.query(sql, user, function (err, res) {             
                    if(err) {
                        console.log(err);
                    }
                    else {
                        console.log('user added');
                    }
            }); 
        }
    })
}
catch (err) {
    console.log(err);
    
}

res.status(201).json({
    message: 'user added',
});
    
    
    
    
});
 



module.exports = router;