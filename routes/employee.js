const express = require('express');
const router = express.Router();
const db = require('../services/mysql.db.service');
const state = db.state;

// employee name and shift

router.get("/", (req, res) => {
    state.connection.query('SELECT * FROM employee LEFT OUTER JOIN duty_hours ON employee.id = duty_hours.employee_id', function(error, results, fields) {
        if(error) {
            console.log(error);
            res.status(404).send("Error occured!");
        } else {
            res.status(200).send([results]);
        }
    });
})


module.exports = router;