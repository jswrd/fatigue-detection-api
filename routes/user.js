const express = require('express');
const router = express.Router();
const db = require('../services/mysql.db.service');
const state = db.state;


//user login
router.post('/login', (req, res, next) => {
    const user_name= req.body.user_name;
    const _password = req.body._password;
    state.connection.query('SELECT * FROM users WHERE user_name = ?',[user_name], function (error, results, fields) {
        if (error) {
            console.log("error ocurred", error);
            res.status(400).json({
                "code": 400,
                "message": "Error ocurred"
            });
        } else {
            
            if(results.length >0){
                if(results[0]._password == _password){
                    req.session.loggedin = true;
                    req.session.user_name = user_name;
                    res.status(200).json({
                        "code": 200,
                        "message": "login sucessfull"
                    });
                    console.log('Logged in!');
                    console.log(req.session.loggedin);
                }
                else {
                    res.status(401).json({
                        "code": 204,
                        "message": "Wrong password"
                    });
                    console.log('Wrong password!')
                }
            }
            else {
                res.status(401).json({
                    "code": 203,
                    "message": "Wrong username"
                });
                console.log('Wrong username!')
            }
        }
    });
})

router.get("/login", (req, res, next) => {
    console.log(req.session.loggedin);
    
    
    if(req.session.loggedin) {
        res.status(200).json({
            status: true
        })
    } else {
        res.status(404).json({
            status: false
        })
    }

})




//user logout
router.get("/logout", (req, res) => {
    req.session.destroy((err) =>{
      if (!err) {
        res.status(200).json({
            "code": 205,
            "message": "Logged out"
        });
        console.log('Logged out');
      }
  });
  });

module.exports = router;