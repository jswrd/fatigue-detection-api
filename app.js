const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const session = require('express-session');

const userRoutes = require('./routes/user');
const empRoutes = require('./routes/employee');
const fRoutes = require('./routes/fScore');
const pyRoutes = require('./routes/pyScore');

//sql database file require
var mysqldb = require('./services/mysql.db.service');

// Connect to MySQL on start
mysqldb.connect(mysqldb.MODE_PRODUCTION, function(err){
    if(!err){
        console.log('mySql is connected!!');
    }
});

// Express-session
app.use(session({
	secret: 'secret',
	resave: false,
    saveUninitialized: true
}));


app.use(morgan('dev'));
app.use('/images', express.static('images'));
app.use(bodyParser.urlencoded({limit: '50mb',extended: false}));
app.use(bodyParser.json({limit: '50mb'}));

// CORS error handler
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Headers', '*');
    res.header('Access-Control-Allow-Credentials', true);
    if(req.method === 'OPTIONS'){
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

// Routes which should handle requests
app.use('/user', userRoutes);
app.use('/employee', empRoutes);
app.use('/fScore', fRoutes);
app.use('/pyScore', pyRoutes);


// Error handler
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});







module.exports = app;